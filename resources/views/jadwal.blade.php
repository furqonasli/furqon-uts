@extends('layouts.main')

@section('container')
<div class="jadwal">
    <h1 class="mt-3 mb-3">Jadawal Sparing Aj Mandiri</h1>
</div>
<div class="tambah-data"><a href="{{route('jdw.create')}} ">
    <button type="button" class="btn btn-primary btn-lg">TAMBAH JADWAL</button>
</a>
</div>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">TIM</th>
        <th scope="col">TGL SPAR</th>
        <th scope="col">ALAMAT</th>
        <th scope="col">TEMPAT</th>
        <th scope="col">AKSI</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <tr>
                <th scope="row">{{ $loop -> iteration}}</th>
                <td>{{ $item -> tim }}</td>
                <td>{{ $item -> tgl_spar }}</td>
                <td>{{ $item -> alamat }}</td>
                <td>{{ $item -> tempat }}</td>
                <td>
                    <form action="{{route('jdw.delete',$item->id)}}" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                    </form>
                    <a href="{{route('jdw.edit', $item->id)}} "><button type="button" class="btn btn-success btn-sm">EDIT</button></a>
                </td>
            </tr>
        @endforeach
      
    </tbody>
  </table>
  @include('sweetalert::alert')
@endsection