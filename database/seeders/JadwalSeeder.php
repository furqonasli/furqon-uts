<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwals')->insert([
            'tim' => 'Ayunda',
            'tgl_spar' => 'Kamis, 20/11/2021',
            'alamat' => 'Sopaah',
            'tempat' => 'Sopaah'
        ]);
    }
}
